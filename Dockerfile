FROM alpine
RUN apk update && apk upgrade
RUN apk add openssh-client openssh iptables

RUN ssh-keygen -t rsa -P '' -f /etc/ssh/ssh_host_rsa_key
RUN mkdir /root/.ssh && \
    ssh-keygen -t rsa -P '' -f /root/.ssh/id_rsa && \
    cp /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys && \
    chmod 600 /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/authorized_keys

ADD start.sh /start.sh

RUN chmod +x /start.sh
EXPOSE 2222

ENTRYPOINT [ "/start.sh" ]
